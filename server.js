let express = require("express"),
    app=express();
let fs = require("fs");
let filename = process.argv[2];
let syncFs =require("./syncFs")
let asyncFs =require("./asyncFs")
let config;
let fileSystem;


fs.readFile(filename, "utf-8", function(err, text) {
    if(!err){
        config = JSON.parse(text);
        if(config.async){
            fileSystem = asyncFs;
        }
        else{
            fileSystem = syncFs;
        }
        app.listen(config.port,(req, res)=>{
    
        })
    }
  });

//создать файл
app.post("/file/:name", (req, res)=>{
    fileSystem.createFile(req.params.name);
    res.send(`file ${req.params.name} crated`);
})

//удалить файл
app.delete("/file/:name", (req, res)=>{

    fileSystem.removeFile(req.params.name);
    res.send(`file ${req.params.name} deleted`);
})

//добавить запись в файл
app.put("/append/file/:name", (req, res)=>{

    console.log(req.query.content);
    fileSystem.appendFile(req.params.name, req.query.content);
    res.send(`text ${req.query.content} append to file  ${req.params.name} `);
})

//перезаписать файл
app.put("/override/file/:name", (req, res)=>{

    fileSystem.writeFile(req.params.name,req.query.content);
    res.send(`text ${req.query.content} override  file  ${req.params.name} `);
})

// переместить файл
app.post("/move/file/:name",(req, res)=>{

    console.log(req.query.newplace);
    fileSystem.moveFile(req.params.name, __dirname + req.query.newplace + req.params.name);
    res.send(`file  ${req.params.name} moved to ${req.query.newplace} `);
})

// скопировать файл
app.post("/copy/file/:name",(req, res)=>{

    console.log(req.query.newplace);
    fileSystem.copyFile(req.params.name, __dirname + req.query.newplace + req.params.name);
    res.send(`file  ${req.params.name} copy to ${req.query.newplace} `);
})
