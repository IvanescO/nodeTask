let fs = require("fs");

exports.createFile = (fname)=>{
   exports.writeFile(fname, '');

}

exports.appendFile = (fname, str)=>{
    fs.appendFileSync(fname, str);
}

exports.writeFile = (fname, str)=>{
    fs.writeFileSync(fname, str);

}

exports.copyFile = (fname, newplace)=>{
    fs.renameSync(fname, newplace);
}
exports.moveFile = (fname, newplace)=>{
    exports.copyFile(fname, newplace)
    exports.removeFile(fname);
}
exports.removeFile = (fname)=>{
    fs.unlinkSync(fname);
}